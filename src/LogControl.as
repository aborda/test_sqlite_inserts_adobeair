package 
{
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	public class LogControl
	{
		public function LogControl()
		{
		}
		
		private static var _instance:LogControl;
		
		public static function getInstance():LogControl
		{
			if (_instance == null)
				_instance = new LogControl();
			
			return _instance;
		}
		
		public function debug(txt:String):void
		{
			send("DEBUG", txt);
		}
		
		public function error(txt:String):void
		{
			send("ERROR", txt);
		}
		
		public function info(txt:String):void
		{
			send("INFO", txt);
		}
		
		public function warning(txt:String):void
		{
			send("WARNING", txt);
		}
		
		public function send(severidad:String, txt:String):void
		{
			var fecha:Date = new Date();
			
			trace(fecha.toLocaleString() + " " + severidad + ": " + txt);
			
			var fileName:String = (fecha.month+1) + "_" + fecha.date + "_" + fecha.fullYear + ".log";
			var logFile:File = File.applicationStorageDirectory.resolvePath(fileName);
			var fileStream:FileStream = new FileStream();
			
			if (logFile.exists)
				fileStream.open(logFile, FileMode.APPEND);
			else
				fileStream.open(logFile, FileMode.WRITE);
			
			fileStream.writeUTFBytes(fecha.toLocaleString() + " " + severidad + ": " + txt + "\n");
			fileStream.close();
		}
	}
}